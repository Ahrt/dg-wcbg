﻿using UnityEngine;
using UnityEditor;

public class PersonDataAsset
{
    [MenuItem("Assets/Create/PersonData")]
    public static void CreateAsset()
    {
        ScriptableObjectUtility.CreateAsset<PersonData>();
    }
}
