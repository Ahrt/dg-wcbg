﻿using UnityEngine;
using UnityEditor;

public class TeamDataAsset
{
    [MenuItem("Assets/Create/TeamData")]
    public static void CreateAsset()
    {
        ScriptableObjectUtility.CreateAsset<TeamData>();
    }
}
