﻿using UnityEngine;
using UnityEditor;

public class PlayerMoneyDataAsset
{
    [MenuItem("Assets/Create/PlayerMoneyData")]
    public static void CreateAsset()
    {
        ScriptableObjectUtility.CreateAsset<PlayerMoneyData>();
    }
}
