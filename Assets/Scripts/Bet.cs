﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Bet : MonoBehaviour {

    public PlayerMoneyData moneyData;
    //public List<Toggle> betToggle = new List<Toggle>();
    //public List<Toggle> betToggle;
    public Toggle[] betToggle;
    public Text[] textList;
    private Logs logHandler;

    public int randNum;
    public int scndNumb;

    private void Start()
    {
        //betToggle = Resources.LoadAll<Toggle>("1a").ToList();
        //betToggle = Resources.LoadAll<Toggle>("").ToList();
        //betToggle = new List<Toggle>(Resources.LoadAll<Toggle>("Toggle"));
        logHandler = GetComponent<Logs>();
    }

    private void Update()
    {
        randNum = (Random.Range(0, 9999) + Random.Range(10000, 123456));
        scndNumb = Random.Range(1, 9999);
        DisplayText();
    }

    public void AddBet()
    {
        if(moneyData.betAmount < moneyData.amount)
        {
            moneyData.betAmount += moneyData.addAmount;
            logHandler.NewActivity("ADIgauysgfyuai");
            logHandler.NewActivity(randNum.ToString());
        }
    }

    public void SubtractBet()
    {
        if(moneyData.betAmount > 0)
        {
            moneyData.betAmount -= moneyData.subtractAmount;
            logHandler.NewActivity("sefdihbsfnj");
        }
    }

    public void SubmitBet()
    {
        if(moneyData.betAmount > 0)
        {
            //moneyData.betList.Add(moneyData.amount -= moneyData.betAmount);
            ToggleOn();
            ToggleOnMinus();
            
        }
        
        moneyData.betAmount = 0;
    }

    void ToggleOn()
    {

        if(betToggle[0].isOn)
        {
            /*Formula for odds still needed 'put them in a new function scrub'*/
            //Test Bets
            /*Get 'Won' teams first
             * after getting won check if player won any bet
             * award with prize if won
             * just lose money if none
             */ 
             
            moneyData.betList.Insert(0, moneyData.betAmount *  2);


            moneyData.amount += moneyData.betList[0];
            moneyData.betList.RemoveAt(0);

        }
        else if(betToggle[1].isOn)
        {
            moneyData.amount -= moneyData.betAmount;
            moneyData.betList.Insert(1, moneyData.betAmount * 3);


            moneyData.amount += moneyData.betList[1];
            moneyData.betList.Remove(1);
        }
        else if (betToggle[2].isOn)
        {
            moneyData.amount -= moneyData.betAmount;
            moneyData.betList.Insert(2, moneyData.betAmount * 3);


            moneyData.amount += moneyData.betList[2];
            moneyData.betList.RemoveAt(2);
        }
        else if (betToggle[3].isOn)
        {
            moneyData.amount -= moneyData.betAmount;
            moneyData.betList.Insert(3, moneyData.betAmount * 3);


            moneyData.amount += moneyData.betList[3];
            moneyData.betList.RemoveAt(3);
        }
        else if (betToggle[4].isOn)
        {
            moneyData.amount -= moneyData.betAmount;
            moneyData.betList.Insert(4, moneyData.betAmount * 3);


            moneyData.amount += moneyData.betList[4];
            moneyData.betList.RemoveAt(4);
        }
        else if (betToggle[5].isOn)
        {
            moneyData.amount -= moneyData.betAmount;
            moneyData.betList.Insert(5, moneyData.betAmount * 3);


            moneyData.amount += moneyData.betList[5];
            moneyData.betList.RemoveAt(5);
        }
 
    }

    void ToggleOnMinus()
    {
        if (betToggle[0].isOn)
        {
            /*Formula for odds still needed 'put them in a new function scrub'*/
            //Test Bets
            /*Get 'Won' teams first
             * after getting won check if player won any bet
             * award with prize if won
             * just lose money if none
             */

            //moneyData.amount -= moneyData.betAmount;

            //moneyData.betList.RemoveAt(0);

        }
    }

    void BetToMoney()
    {
        //Add odds here
        moneyData.amount -= moneyData.betAmount;
        moneyData.betList.Insert(0, moneyData.betAmount * 3);

    }

    void DisplayText()
    {
        textList[0].text = "Player Money: " + moneyData.amount.ToString();
        textList[1].text = "Bet Amount: " + moneyData.betAmount.ToString();
    }


}