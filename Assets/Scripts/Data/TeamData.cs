﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeamData : ScriptableObject {

    public string teamName;
    public string shortName;

    public int teamAttack;
    public int teamDefense;

    public int teamPower;
    public int totalGoals;
    public int currentGoals;

    public int teamDraws;
    public int teamWins;
    public int teamLosses;

    public float teamModifier;

    public void TeamDataObject()
    {

    }
}

/*

*/