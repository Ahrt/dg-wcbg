﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonData : ScriptableObject {

    public string personName;
    public string country;
    public string position;

    public int personAttack;
    public int personDefense;

    public int pacing;
    public int shooting;
    public int passing;
    public int dribbling;
    public int physical;
    
}
