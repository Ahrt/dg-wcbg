﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoneyData : ScriptableObject
{
    public int amount;
    public int betAmount;

    public int addAmount;
    public int subtractAmount;

    public int winnings;
    public List<int> betID = new List<int>();
    public List<int> uniqueNumbers = new List<int>();
    public List<int> betList = new List<int>();
}
