﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

using UnityEngine;

//Test script for adjusting values
public class ManipulateValue : MonoBehaviour {

    public TeamData[] teamData;
    public Text[] infoText;
    public Toggle[] toggle;

    public float[] numberValues;

    public PlayerMoneyData moneyData;
    float moneyPool;

    
    private void Start()
    {
        numberValues[0] = teamData[0].teamPower;
        numberValues[1] = teamData[1].teamPower;

        numberValues[2] = moneyData.amount;
        numberValues[3] = moneyData.addAmount;
        numberValues[4] = moneyData.subtractAmount;
        numberValues[5] = moneyData.betAmount;

        InfoText();
    }
    
    public void Add()
    {
        numberValues[0] += 2;
        numberValues[1] += 3;

        InfoText();
        //Adds to scriptable object value permanently
        //teamData.teamPower += 1;
    }

    public void Minus()
    {
        numberValues[0] -= 2;
        numberValues[1] -= 4;

        InfoText();
    }

    public void FightTest()
    {
        if(numberValues[0] > numberValues[1])
        {
            Debug.Log("Team Wins");
            if(toggle[0].isOn)
            {
                moneyPool *= 2f;
                numberValues[2] += moneyPool;
                moneyPool = 0;
            }
            
        }
        else if (numberValues[0] < numberValues[1])
        {
            Debug.Log("Rival Wins");
            if (toggle[1].isOn)
            {
                moneyPool *= 2f;
                numberValues[2] += moneyPool;
                moneyPool = 0;
            }
        }
        else if (numberValues[0] == numberValues[1])
        {
            Debug.Log("DRAW");
            if (toggle[2].isOn)
            {
                moneyPool *= 2f;
                numberValues[2] += moneyPool;
                moneyPool = 0;
            }
        }

        
    }

    void InfoText()
    {
        infoText[0].text = "Power: " + numberValues[0].ToString();
        infoText[1].text = "Power: " + numberValues[1].ToString();

        infoText[2].text = "Money: " + numberValues[2].ToString();
        infoText[3].text = "Bet: " + numberValues[5].ToString();
    }

    public void ActiveToggle()
    {
        if (toggle[0].isOn)
        {
            Debug.Log("1");
        }
        else if (toggle[1].isOn)
        {
            Debug.Log("2");
        }
        else if (toggle[2].isOn)
        {
            Debug.Log("3");

        }
    }
    
    public void BetSubmit()
    {
        numberValues[2] -= numberValues[5];
        moneyPool = numberValues[5];

        InfoText();
        ActiveToggle();
    }

    public void BetReset()
    {
        numberValues[5] = 0;
    }

    public void BetAdd()
    {
        if(numberValues[5] < numberValues[2])
        {
            numberValues[5] += numberValues[3];
        }

        InfoText();
    }

    public void BetMinus()
    {
        if(numberValues[5] > 0)
        {
            numberValues[5] -= numberValues[3];
        }

        InfoText();
    }

    private void Update()
    {
        InfoText();
    }

}
