﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Class for simulating battles
public class SimulateMatch : MonoBehaviour {
    //public TeamData[] teamData;
    public List<TeamData> teamData = new List<TeamData>();
    public List<string> ListTeam = new List<string>();

    public int[] testArray = new int[10];
    public int numTeams = 8;
    int count;

    private void Start()
    {
        
        // MatchSchedule();
        // ListMatches();
        ValueResetter();
        
        if (teamData[0].teamPower > teamData[1].teamPower)
        {
            teamData[0].teamModifier += 1;
        }

       else if(teamData[0].teamPower < teamData[1].teamPower)
        {
            teamData[1].teamModifier += 1;
        }

        Match();
    }

    private void Update()
    {
        while(count < 10)
        {
            ValueResetter();

            if (teamData[0].teamPower > teamData[1].teamPower)
            {
                teamData[0].teamModifier += 1;
            }

            else if (teamData[0].teamPower < teamData[1].teamPower)
            {
                teamData[1].teamModifier += 1;
            }

            Match();
            count++;
        }

    }

    public void Match()
    {

        for (int i = 0; i < 90; i++)
        {
            int rand = Random.Range(1, 100);
            ComebackBonus();
            if (rand <= 1 + teamData[0].teamModifier)
            {
                //Debug.Log("GOAL!");
                teamData[0].currentGoals += 1;
                teamData[0].totalGoals += 1;
            }
            
        }

        for (int i = 0; i < 90; i++)
        {
            int rand = Random.Range(1, 100);
            ComebackBonus();
            if (rand <= 1 + teamData[1].teamModifier)
            {
                //Debug.Log("GOAL!");
                teamData[1].currentGoals += 1;
                teamData[1].totalGoals += 1;
            }
        }

         DetermineWinner();
        
    }

    public void ComebackBonus()
    {
        for(int i = 0; i <= 1; i++)
        {
            if(teamData[i].teamModifier == 0 && teamData[i].currentGoals == 0)
            {
                teamData[i].teamModifier = 1;
            }
        }
    }

    public void DetermineWinner()
    {
        if(teamData[0].currentGoals > teamData[1].currentGoals)
        {
            Debug.Log("1Eyey");
            teamData[0].teamWins += 1;
            teamData[1].teamLosses += 1;
        }
        else if (teamData[0].currentGoals < teamData[1].currentGoals)
        {
            Debug.Log("2Eyey");

            teamData[1].teamWins += 1;
            teamData[0].teamLosses += 1;
        }
        else if (teamData[0].currentGoals == teamData[1].currentGoals)
        {
            Debug.Log("3Eyey");

            teamData[0].teamDraws += 1;
            teamData[1].teamDraws += 1;
        }

        ValueResetter();
    }

    public void ValueResetter()
    {
        for (int i = 0; i <= 1; i++)
        {
            teamData[i].teamModifier = 0;
            teamData[i].currentGoals = 0;
        }
    }


//---------------------------------------------------------------------------------//
    public void MatchSchedule()
    {
        /*if(teamData.Count % 2 != 0)
        {
            Debug.Log("EY");
        }*/

        for (int i = 0; i < 13; ++i)
            testArray[i % testArray.Length] = i;
    }
    
    public void RoundRobin()
    {
        if(teamData.Count % 2 != 0)
        {
            Debug.Log("EYYEY");
        }

        int numDays = (numTeams - 1);
        int halfSize = numTeams / 2;

        teamData.AddRange(teamData);
        teamData.RemoveAt(0);

        int teamSize = teamData.Count;

        for(int day = 0; day < numDays; day++)
        {
            //Function
            int teamIdx = day % teamSize;

            for(int idx = 1; idx < halfSize; idx++)
            {
                int firstTeam = (day + idx) % teamSize;
                int secondTeam = (day + teamSize - idx) % teamSize;
            }
        }
    }

    public void ListMatches()
    {
        if (ListTeam.Count % 2 != 0)
        {
            ListTeam.Add("Bye");
            Debug.Log("Ey");
        }

        int numDays = (numTeams - 1);
        int halfSize = numTeams / 2;

        List<string> teams = new List<string>();

        // teams.AddRange(ListTeam.Skip(halfSize).Take(halfSize));
        // teams.AddRange(ListTeam.Skip(1).Take(halfSize -1).ToArray().Reverse());

        teams.AddRange(ListTeam); // Copy all the elements.
        teams.RemoveAt(0); // To exclude the first team.

        int teamsSize = teams.Count;

        for (int day = 0; day < numDays; day++)
        {
            Debug.Log(string.Format("Day {0}", (day + 1)));

            int teamIdx = day % teamsSize;

            Debug.Log(string.Format("{0} vs {1}", teams[teamIdx], ListTeam[0]));

            for (int idx = 1; idx < halfSize; idx++)
            {
                int firstTeam = (day + idx) % teamsSize;
                int secondTeam = (day + teamsSize - idx) % teamsSize;
                Debug.Log(string.Format("{0} vs {1}", teams[firstTeam], teams[secondTeam]));
            }
        }
    }



}
