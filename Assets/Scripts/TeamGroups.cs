﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeamGroups : MonoBehaviour {

    public List<TeamData> teams;

    public List<TeamData> group1, group2, 
        group3, group4, group5, group6, group7, group8;

    public int numTeams = 8;
    //public List<TeamData> teamData = new List<TeamData>();

    void Start()
    {
        teams = TeamManager.Instance.getTeamData();
        AssignToGroups();
        RoundRobinTest();
    }

    public void AssignToGroups()
    {
        group1 = teams.GetRange(0, 4);
        group2 = teams.GetRange(4, 4);
        group3 = teams.GetRange(8, 4);
        group4 = teams.GetRange(12, 4);
        group5 = teams.GetRange(16, 4);
        group6 = teams.GetRange(20, 4);
        group7 = teams.GetRange(24, 4);
        group8 = teams.GetRange(28, 4);

    }

    public void RoundRobinTest()
    {
        if (group1.Count % 2 != 0)
        {
            //Just in case of odd numbered teams
        }

        int numDays = (numTeams - 1);
        int halfSize = numTeams / 2;

        List<TeamData> g1Teams = new List<TeamData>();


        g1Teams.AddRange(group1);
        g1Teams.RemoveAt(0);

        int groupSize = g1Teams.Count;

        for (int day = 0; day < numDays; day++)
        {
            Debug.Log(string.Format("Day {0}", (day + 1)));

            int teamIdx = day % groupSize;

            Debug.Log(string.Format("{0} vs {1}", g1Teams[teamIdx], group1[0]));

            for (int idx = 1; idx < halfSize; idx++)
            {
                int firstTeam = (day + idx) % groupSize;
                int secondTeam = (day + groupSize - idx) % groupSize;
                Debug.Log(string.Format("{0} vs {1}", 
                    g1Teams[firstTeam], g1Teams[secondTeam]));
            }
        }
    }
}
