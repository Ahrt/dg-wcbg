﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Person : MonoBehaviour {

    public PersonData pdata;

    public Person()
    {
        pdata.personName = "Default";
        pdata.personAttack = 0;
        pdata.personDefense = 0;
    }

}
