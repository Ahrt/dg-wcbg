﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Team
{
    public TeamData teamData;

    public string teamName;
    public int power;

    public Team()
    {
        teamName = teamData.teamName;
        power = teamData.teamPower;
    }
}
