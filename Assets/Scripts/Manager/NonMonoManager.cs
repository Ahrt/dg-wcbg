﻿using UnityEngine;
using System.Collections;

public class NonMonoManager<T> where T: class, new()
{
    protected static T s_instance = null;

	public static T Instance { get 
		{ 
			Create();
			return s_instance; 
		} 
	}

    public static void Create ()
    {
        if (s_instance == null)
        {
            s_instance = new T ();
            
        }
    }

    public virtual void Destroy ()
    {
        s_instance = null;
        
    }
}
