﻿using System.Collections;
using System.Threading;
using System.Collections.Generic;
using UnityEngine;

public class TeamManager : MonoManager<TeamManager> {

    public List<TeamData> teamData;
    private static Random rng = new Random();

    public List<TeamData> getTeamData()
    {
        ShuffleTeams();
        return teamData;
    }

    void Start()
    {
        
    }

    void ShuffleTeams()
    {
        for (int i = 0; i < teamData.Count; i++)
        {
            int randomIdx = i + (int)(Random.Range(0.0f, 1.0f) * (teamData.Count - i));
            var temp = teamData[randomIdx];
            teamData[randomIdx] = teamData[i];
            teamData[i] = temp;
        }
    }
}
