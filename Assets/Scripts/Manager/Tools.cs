﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;
using System.Net;
using System.Collections.Generic;

public static class Tools
{
    public static System.Collections.IEnumerator WaitForRealTime (float p_delay)
    {
        while (true)
        {
            float pauseEndTime = Time.realtimeSinceStartup + p_delay;
            while (Time.realtimeSinceStartup < pauseEndTime)
            {
                yield return 0;
            }
            break;
        }
    }

    public static T InstantiateCanvas<T> (string p_name, Transform p_parent = null) where T:MonoBehaviour
    {
        return InstantiateResource<T> ("Prefabs/Canvas/" + p_name, p_parent);
    }

    public static GameObject InstantiateResource (string p_path, Transform p_parent = null)
    {
        Object resourceObj = Resources.Load (p_path);
        if (resourceObj == null)
        {

            return null;
        }
		
        GameObject gameObj = GameObject.Instantiate (resourceObj) as GameObject;
        if (gameObj == null)
        {
  
            return null;
        }
		
        gameObj.name = Tools.RemoveSubstring (gameObj.name, "(Clone)");
		
        if (p_parent != null)
        {
            gameObj.transform.SetParent (p_parent);
        }
		
        return gameObj;
    }

    public static T InstantiateManager<T> (string p_name, Transform p_parent = null) where T:MonoBehaviour
    {
        //Debug.Log(p_name);
        return InstantiateResource<T> ("Prefabs/Managers/" + p_name, p_parent);
    }

    public static T InstantiateResource<T> (string p_path, Transform p_parent = null) where T:MonoBehaviour
    {
        T comp = InstantiateResource (p_path, p_parent).GetComponent<T> ();

        if (comp == null)
        {

            return null;
        }
		
        return comp;
    }

    public static string RemoveSubstring (string p_mainStr, string p_subStr)
    {
        p_mainStr = p_mainStr.Replace (p_subStr, "");
        p_mainStr = p_mainStr.Trim ();
        return p_mainStr;
    }

    public static bool IsMultipleTouch ()
    {
        return (Input.touchCount > 1);
    }

    public static bool CheckForInternet ()
    {
        NetworkReachability reachability = Application.internetReachability;
		bool return_value = false;
        switch (reachability)
        {
            case NetworkReachability.ReachableViaCarrierDataNetwork:
				return_value = true;
                break;
            case NetworkReachability.ReachableViaLocalAreaNetwork:
				return_value = true;
                break;
        }


		return return_value;
    }

	public static string ConvertChips (ulong chips)
	{
		string strChips = chips.ToString("N0");

		// cap 10k
		if (chips <= 999L) {
			return strChips;
		}

		string[] chip = strChips.Split(","[0]);
		string formatted = string.Empty;

		string subValue = strChips;
		if (chip.Length > 1) {
			string doubleDigit = chip[1].Substring(0, 2);
			if(doubleDigit.ToCharArray()[0] == '0' && doubleDigit.ToCharArray()[1] == '0'){
				subValue = string.Empty;
			} else if (doubleDigit.ToCharArray () [1] == '0') {
				subValue = "." + doubleDigit.ToCharArray () [0].ToString ();
			} else {
				subValue = "." + doubleDigit;
			}
		}

		switch (chip.Length) {
		case 2:
			formatted = chip[0] + subValue + "K";
			break;
		case 3:
			formatted = chip[0] + subValue + "M";
			break;
		case 4:
			formatted = chip[0] + subValue + "B";
			break;
		case 5:
			formatted = chip[0] + subValue + "T";
			break;
		default:
			formatted = strChips;
			break;
		}

		return formatted;
	}

	public static string NumberFormatMillions (long chips) {

		string strChips = chips.ToString("C0");


		// cap 10k
		if (chips <= 999999L) {
			return strChips;
		}

		string[] chip = strChips.Split(","[0]);
		string formatted = string.Empty;

		string subValue = strChips;
		if (chip.Length > 1) {
			string singleDigit = chip[1].Substring(0, 1);
			subValue = singleDigit.Equals("0") ? string.Empty : "." + singleDigit;
		}

		//Debug.LogErrorFormat("Extensions::ChipsFormat Value:{0} Chip[0]:{1} SubValue:{2}\n", chips, chip[0], subValue);

		switch (chip.Length) {
		case 2:
			formatted = chip[0] + subValue + "K";
			break;
		case 3:
			formatted = chip[0] + subValue + "M";
			break;
		case 4:
			formatted = chip[0] + subValue + "B";
			break;
		case 5:
			formatted = chip[0] + subValue + "T";
			break;
		default:
			formatted = strChips;
			break;
		}

		return formatted;
	}

    public static bool IsConnected (string URL)
    {
        // Reference: http://answers.unity3d.com/questions/567497/how-to-100-check-internet-availability.html
        try
        {
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create (URL);
            request.Timeout = 5000;
            request.Credentials = CredentialCache.DefaultNetworkCredentials;
            HttpWebResponse response = (HttpWebResponse) request.GetResponse ();
			
            if (response.StatusCode == HttpStatusCode.OK)
                return true;
            else
                return false;
        }
        catch
        {
            return false;
        }
    }
}
