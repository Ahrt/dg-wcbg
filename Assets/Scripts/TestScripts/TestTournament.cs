﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestTournament : MonoBehaviour
{
    public TeamGroups tG;
    public List<string> ListTeam = new List<string>();
    public int numTeams = 8;

    // Use this for initialization
    void Start()
    {
        //tG.teams = TeamManager.Instance.getTeamData();
        ListMatches();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void RoundRobinTournament()
    {
        

    }

    //---
    public void ListMatches()
    {
        if (ListTeam.Count % 2 != 0)
        {

        }

        int numDays = (numTeams - 1);
        int halfSize = numTeams / 2;

        List<string> teams = new List<string>();

        // teams.AddRange(ListTeam.Skip(halfSize).Take(halfSize));
        // teams.AddRange(ListTeam.Skip(1).Take(halfSize -1).ToArray().Reverse());

        teams.AddRange(ListTeam); // Copy all the elements.
        teams.RemoveAt(0); // To exclude the first team.

        int teamsSize = teams.Count;

        for (int day = 0; day < numDays; day++)
        {
            Debug.Log(string.Format("Day {0}", (day + 1)));

            int teamIdx = day % teamsSize;

            Debug.Log(string.Format("{0} vs {1}", teams[teamIdx], ListTeam[0]));

            for (int idx = 1; idx < halfSize; idx++)
            {
                int firstTeam = (day + idx) % teamsSize;
                int secondTeam = (day + teamsSize - idx) % teamsSize;
                Debug.Log(string.Format("{0} vs {1}", teams[firstTeam], teams[secondTeam]));
            }
        }

    }




}