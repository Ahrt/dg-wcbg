﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomNumbers : MonoBehaviour {

    public Text testText;
    public int winCount;

	// Use this for initialization
	void Start () {
        for (int i = 0; i < 90; i++)
        {
            int rand = Random.Range(1, 100);
            Debug.Log(rand);

            if (rand <= 33)
            {
                winCount += 1;
            }
            DisplayText();
        }
    }
	
	// Update is called once per frame
	void Update () {
       
    }

    void DisplayText()
    {
        testText.text = "No. of Wins: " + winCount.ToString();
    }
}
